/*
	Faça um algoritmo que receba uma matriz com números inteiros (positivos e negativos), compute e exiba o maior elemento da matriz 
	com sua respectiva posição (linha e coluna). Caso este elemento 
	apareça várias vezes, deve-se exibir todas as posições que o mesmo se encontra.
*/

var mat = [
	[-10, 20, 30, 40],
	[11, -21, 42, 41],
	[12, 22, -32, 42],
	[13, 23, 42, -43],
];
function verificaMatriz (mat) {
	var maiorElemento = 0;
	var maiorPos = '';

	/**2 for para ler a matriz;*/
	for (var i = 0; i < mat.length; i++) {
		for (var j = 0; j < mat[i].length; j++) {

			if (mat[i][j] > maiorElemento) { //descobrindo qual o maior elemento da matriz;
				/**if ternario;*/
				maiorPos = mat[i][j] == maiorElemento ? maiorPos += `pos[${i}][${j}]` : `pos[${i}][${j}]`;
				/**maiorPos = `pos[${i}][${j}]`; //concatenação das posições;*/ 
				maiorElemento = mat[i][j];
			}
		}
	}
	console.log(`Maior elemento: ${maiorElemento}\n ${maiorPos}`);
}
verificaMatriz(mat);