/*
Faça um algoritmo que receba a 
[temperatura média] 
de cada mês do ano e
[armazene essas temperaturas em um vetor]; ok
[calcule e exiba a maior e a menor temperatura] - ok
do ano e 
[em que mês estas temperaturas aconteceram]. ok
*/

let leitura = require('readline-sync');

let temperaturas = [];
let maiorTemp;
let menorTemp;
let maiorMes;
let menorMes;

for (var mes = 0; mes < 12; mes++) {
	let temp = parseInt(leitura.question(`Temperatura do mês ${mes + 1}: `));
	temperaturas.push(temp);
	if(mes == 0) {
		menorMes = mes + 1;
		menorTemp = temperaturas[0];

		maiorMes = mes + 1;
		maiorTemp = temperaturas[0];
	} else {
		if(temperaturas[mes] < menorTemp) {
			menorMes = mes + 1;
			menorTemp = temperaturas[mes];
		}
		if(temperaturas[mes] > maiorTemp) {
			//Se a minha temperatura que esta no loop da interação for 
			maiorMes = mes + 1;
			maiorTemp = temperaturas[mes];
		}
	}
} 

console.log(`
	Menor Temperatura: ${menorTemp}, ocorreu no mês ${menorMes}. \n
	Maior Temperatura: ${maiorTemp}, ocorreu no mês ${maiorMes}.
	`);
// console.log(`Maior Temperatura: ${maiorTemp}, ocorreu no mês ${maiorMes}.`);