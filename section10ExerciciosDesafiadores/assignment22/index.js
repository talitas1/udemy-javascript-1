/**
 * Faça um algoritmo que receba uma "matriz" com números inteiros, "calcule e exiba a soma" dos elementos da diagonal principal.
 */
let matriz = [
	[10, 20, 30, 40],
	[10, 20, 30, 40],
	[10, 20, 30, 40],
	[10, 20, 30, 40]
]
/*
	A matriz nada mais é que um vetor, com mais linhas e colunas;
*/
let totalDiagonal = 0;
for (var i = 0; i < matriz.length; i++) {
	for (var j = 0; j < matriz[i].length; j++) {
		if (i == j) {
			totalDiagonal += matriz[i][j]; //totalDiagonal = totalDiagonal + matriz[i][j];
		}
	}
}
console.log(totalDiagonal);