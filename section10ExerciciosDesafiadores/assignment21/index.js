/**
 * Faça um algoritmo que ao ser conhecido 
 * um [vetor]
 * e um {elemento} 
 * insira ordenadamente o elemento
	no vetor.
*/

var vetor = [8,12,15,20,17];
var elemento = 9; //à ser inserido no vetor conforme enuciado;

*
function exibeVetor (vector) {
	for (i = 0; i < vector.length; i++) 
		console.log(vector[i]);
}

function insereOrdenado (vet, ele) {
	var inseriu = false;

	/*
	Criar uma repetição (um for) que verificara cada item do vetor;
	- criar contador (i), 
	- o mesmo percorrera cada elemento do vetor(.length);
	- encremente o i++;
	*/
	
	for (var i = 0; i < vet.length && !inseriu; i++) {
	 	
	 	if (ele < vet[i]) {
	 		/* Se o elemento (5) for < que 8 coloca o mesmo na posição
	 			ordenada(conforme o enunciado pede);
	 		*/
	 		
	 		/* Este segundo for vai percorrer o vetor e pôr todos elementos 		para trás do vetor(j--);
	 		*/
	 		for (var j = vet.length; j > i; j--)
	 			/*
	 		 	A variavel J vai começar do final do vetor;
	 		 	E ele vai andar ate quando ele for menor 
	 		 	que a variavel i do 1º for. 
	 			*/
	 			vet[j] = vet[j-1];
	 		vet[i] = ele;
	 		inseriu = true;
	 	}
	}
	if (!inseriu)
		vet.push(ele); /*na ultima posição do vetor;*/
		/*console.log(vet);*/
	exibeVetor(vet);
}

/*function insereOrdenado (vet, ele) {
	vet.push(ele); // push para inserir um valor(não ordenado);
	/**
	 *	sort sorteio, de ordenação; algoritmo próprio do JavaScript;  
	 *  
	 * vet = vet.sort(function(a,b) {
		return a - b;
	})
		Ou, outra forma  mais enxuta é usar o Arrow functions (==>);
	*

	vet = vet.sort((a,b) => a - b)

	console.log(vet)
}*/
	
insereOrdenado(vetor, elemento);