const pessoa = {
	nome: "Talita",
	idade: "24",
	body:"qualquer"
}

const x = pessoa;
const {idade} = pessoa;

console.log("sem desconstrutor: ", x.idade);
console.log("com desconstrutor: ", idade);