/*
Modulo para imagem
Modulo para capturar dados da web
1º vamos instalar algumas dependencias, alguns pacotes.
	Got - de PEGAR; pega dados da web (npm i got).
	Terminal Image -  pacote servi para capturar e exibir na tela em modo texto mesmo (npm i terminal-image).
*/

// const terminalImage = require("terminal-image");
import terminalImage from 'terminal-image';
// const got = require("got");
import got from 'got';

async function showImage(image){
	try {
		const body = await got.get(image
			).buffer();
		// console.log(body)
		console.log(await terminalImage.buffer(body));
	} catch (error) {
		console.error(error);
	}
}

async function getAPOD(){
	const dados = await got('https://api.nasa.gov/planetary/apod?api_key=gzQ9oe1KEFYluywSvdYi0Li3jqATMC4vO94vefbf&date=2019-08-05'
		,{
		}
		);
	let apodImage = dados.body;
	const {url} = JSON.parse(apodImage);
	console.log(url);
	showImage(url);
}
// Pegar referencia da img, url na nasa (APOD)
getAPOD();