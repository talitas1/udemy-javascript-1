// /*Programa variável, concatenação e, templates literals.*/

// var idade = 24;
// var text = `Eu Talita, tenho ${idade} anos.`;

// console.log(text);

/*Programa variável, concatenação.*/

var idade = 24;
var anoAtual = 2021;
var anoDeNascimento = anoAtual - idade;

console.log('Eu Talita, nasci em ' + anoDeNascimento);
