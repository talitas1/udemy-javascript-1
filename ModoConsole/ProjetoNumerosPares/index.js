/*variáveis globais*/
let readlineSync = require('readline-sync');
let opmenu;
/*Vetor*/
let numeros = new Array();

/*Função que devolve um valor booleano true(verdadeiro) ou false(falso)...*/
function isPar(n){
	let par = false;
	if(n % 2 == 0){
		par = true;
	}
	return par;
}

/*Fazer a leitura do usuário...*/
function lerNumber(){
	let n = parseInt(readlineSync.question('\nDigite um número: '));
	return n;
}

/*Usando função dentro de outras funções...*/
function inserir(){
	/*variável local*/
	let n = lerNumber();
	/*if(isPar(n) == true)*/
	if(isPar(n) == true){
		/*Inserindo um número dentro do Array... */
		if((jaExiste(numeros, n))){
			console.log('\nErro, esse valor já foi inserido');
		} else {
			numeros.push(n);
		}
	} 
}

function listar(){
	for(let i = 0;i < numeros.length; i++){
		console.log(`\n[ ${i} ] = ${numeros[i]}`);
	}
}

function localizar(n){
	let pos = -1;
	let i = 0;
	while(i < numeros.length && numeros[i] != n){
		i++;
	}
	if(numeros[i] == n){
		pos = i;
	}
	return pos;
}

/*Opções do sistema
1 - Inserir um número - ok
2 - Listar os números cadastrados - ok

3 - Excluir um número
4 - Alterar um número do array
5 - Localizar um número no array - ok
0 - Sair
*/

function exibirMenu(){
	console.log('\nSistema de controle de números pares.');
	console.log('\n1 - inserir um número.  ');
	console.log('2 - Listar os números cadastrados.  ');
	console.log('3 - Excluir um número. ');
	console.log('4 - Alterar um número do array. ');
	console.log('5 - Localizar um número no array. ');
	console.log('0 - Sair. ');
	/*variável local... só vai existir nesta função.*/
	let retorno = parseInt(readlineSync.question('\nDigite uma opção: '));
	return retorno;
}

function jaExiste(lista, n){
	if (lista.includes(n)){
		return true;
	}else{
		return false;
	}
}

while(opmenu != 0){
	opmenu = exibirMenu();

	if(opmenu == 1){
		inserir();
	}

	if(opmenu == 2){
		listar();
	}

	if(opmenu == 5){
		let n = parseInt(readlineSync.question('\nDigite um número para ser localizado: '));
		/*let pos = localizar(n);*/
		let pos = numeros.indexOf(n);
		if(pos != -1){
			console.log(`\n => Valor ${n} foi encontrado na posição: ${pos}.`);
		}else {
			console.log('\nValor não encontrado.');
		}
	}
}
console.log('\nObrigado por utilizar o nosso sistema.');