var readline = require("readline-sync");
let canivete = require("canivetesuiconodelita");
var n = 0;
// resto = 0;
var resposta = false;

console.log('Verifica se um numero informado e par ou impar.');
n = parseInt(readline.question('Informe um numero inteiro: '));

// Variavél resto recebe n porcentagem 2. 
// Comando divide o valor contido em n por 2 (retorna o resto da divisão).
// resto = n%2;
resposta = canivete.NumeroPar(n);

// Operadores lógicos: == (IGUAL), < (MENOR QUE),> (MAIOR QUE), 
// >= (MAIOR OU IGUAL), <= (MENOR OU IGUAL), e 
// != (DIFERENTE).

// Se conteudo de resto for igual a 0...
// if(resto == 0){
if(resposta == true){
	console.log('O numero informado e par.')
} else {
	console.log('O numero informado e ímpar. ');
}

// if(resto != 0){
// 	console.log('O número informado é impar.')
// }