var readlineSync = require('readline-sync');
// Variavél do tipo string ""
var nome = "";
var n1 = 0;
var n2 = 0;
var n3 = 0;
var n4 = 0;
var media = 0;

//Entrada de dados
console.log('PROGRAMA QUE CALCULA A MEDIA DE UM ALUNO.');
nome = readlineSync.question('Qual seu nome? ' );
n1 = parseFloat(readlineSync.question('1ª NOTA: '));
n2 = parseFloat(readlineSync.question('2ª NOTA: '));
n3 = parseFloat(readlineSync.question('3ª NOTA: '));
n4 = parseFloat(readlineSync.question('4ª NOTA: '));

media = (n1 + n2 + n3 + n4)/4;

// Comando de decisão, onde ele vai checar a condição que você estipulou dentro do parenteses...
if(media < 5) {
	console.log(`${nome} sua média é ${media}, você foi REPROVADO(a)!`);
} else {
	console.log(`${nome} sua média é ${media}, você foi APROVADO(a)!`);
}
