//Programa que exibe a tabuada número
// armazenado na variavel de nome N.

var readlineSync = require('readline-sync');

console.log('Programa que exibe a tabuada de um número.');

var n = parseInt(readlineSync.question('Informe um número: '));

console.log('Tabuada do número ' + n);

console.log(n +'x 1 = ' + (n*1));
console.log(n +'x 2 = ' + (n*2));
console.log(n +'x 3 = ' + (n*3));
console.log(n +'x 4 = ' + (n*4));
console.log(n +'x 5 = ' + (n*5));
console.log(n +'x 6 = ' + (n*6));
console.log(n +'x 7 = ' + (n*7));
console.log(n +'x 8 = ' + (n*8));
console.log(n +'x 9 = ' + (n*9));
console.log(n +'x 10 = ' + (n*10));
