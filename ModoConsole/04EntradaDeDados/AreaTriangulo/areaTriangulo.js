var readline = require('readline-sync');
var calculaarea = require('area-triangulo-df');
// Uso do 'require' para solicitar a agregação desse modulo: 'area-triangulo-df'.
/*
var base = 0;
var altura = 0;
var area = 0;
*/
console.log('CALCULA A AREA DE UM TRIANGULO.');
var base = parseFloat(readline.question('Base: '));
//para receber do usuario números quebrados exemplo: 10,5
var altura = parseFloat(readline.question('Altura: '));
var area = calculaarea(base, altura);
console.log("A area do triangulo é: " + area);
console.log('FIM');