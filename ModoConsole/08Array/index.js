
var familia = ["Danilo", "Samira", "Alice","Livia"];
familia[4] = "Mel";
/*Adicionando um novo elemento no array, e apartir de onde*/ 
familia.push("Sueli");
/*Novo objeto, novo elemento.*/
var tl = familia.length;
/*Tamanho lógico, variavel vai receber tamanho do array.*/
// familia [tl] = "Gerson";
console.log(familia);
console.log(tl);

/*var pessoa = familia [0];
console.log(familia);
console.log(pessoa);*/

/*var numeros = new Array();
numeros.push(1);
numeros.push(22);
numeros.push(12);
numeros.push(24);
numeros.push(4);
numeros.push(26);
numeros.push(2);
console.log(`Tamanho: ${numeros.length}. Valores dos vetores: ${numeros}`);*/