var readlineSync = require('readline-sync');
/*variavel objeto de leitura do usuario.*/

var numeros = new Array();
/*vetor de numeros, vazio.*/
var n = 10;
/*efetua a leitura dos usuarios, entradas.*/

/*enquanto n for diferente de 0 execute; quando ele for igual a 0 ele sai do laço.*/
while (n != 0) {
	console.log(`Leitura de numeros impares.`);	
	console.log(`Para sair digite zero(0).`);
	n = parseInt(readlineSync.question('Informe um numero: '));
	if ((n!=0) && (n%2==1)) {
		/*se(if) n for !=(diferente) de zero e(&&) n (%)dividido por 2 for ==(igual) a 1 sai do if.*/
		numeros.push(n);
		/*Array(numeros) colocar na 1ª posição o que esta dentro do N.*/
	}
}

console.log(`Valores inseridos no Array: ${numeros}`);