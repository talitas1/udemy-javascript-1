/*
Simulando uma tela de cadastro, faça a leitura de uma senha,
e de sua confirmação, 
verifique se ambas são iguais 
e autorizado igual a "ADM", se sim, imprima "ok".
 */

var readlineSync = require('readline-sync');
/*Variavel do tipo string ""*/

/*var senha = 0;
var confirmandoSenha = "";*/

/*Entrada de dados.*/
console.log('Programa mostra tela do usuario.');

var senha = readlineSync.question('Insira sua senha: ' );
var confirmandoSenha = readlineSync.question('Confirme sua senha: ');
var autorizada = readlineSync.question('Autorize: ');

if(senha == confirmandoSenha && autorizada == 'ADM') {
	console.log('Ok!');
} else {
	console.log(`Senha não autorizada.`)
}