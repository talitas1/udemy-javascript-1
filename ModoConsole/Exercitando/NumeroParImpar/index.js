/*
Faça um programa que LEIA um número inteiro
e 'se' ele for par EXIBA o número digitado 10 vezes, 
'se' for impar EXIBA o número digitado 5 vezes (utilizando o comando while).
*/

var readlineSync = require('readline-sync');

console.log('Programa que le numero par e impar.');

var n = parseInt(readlineSync.question('Informe um numero inteiro: '));
var i = 1;
while ((n % 2 == 0) && (i <= 10)) {
	console.log(n);
	i++;
}

while ((n % 2 != 0) && (i <= 5)) {
	console.log(n);
	i++;
}