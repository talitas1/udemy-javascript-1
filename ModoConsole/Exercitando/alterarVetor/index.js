// Variáveis globais.
let readlineSync = require('readline-sync');
let opmenu;
// Vetor.
let numeros = new Array();

// Função que devolve um valor boolean true(verdadeiro) ou false(falso).
function IsPar(n){
	let par = false;
	if(n % 2 == 0){
		par = true;
	}
	return par;
}

// Faz a leitura do usuário.
function LerNumber(){
	let n = parseInt(readlineSync.question('\nDigite um número: '));
	return n;
}

// Usando função dentro de outras funções.
function Inserir(){
	//Variável local.
	let n = LerNumber();
	if(IsPar(n) == true){
		// Inserindo um número dentro do Array.
		if((jaExiste(numeros, n))){
			console.log('\nErro, esse valor já foi inserido');
		} else {
			numeros.push(n);
		}
	} 
}

function listar(){
	for(let i = 0;i < numeros.length; i++){
		console.log(`\n[ ${i} ] = ${numeros[i]}`);
	}

}

function localizar(n){
	let pos = -1;
	let i = 0;
	while(i < numeros.length && numeros[i] != n){
		i++;
	}
	if(numeros[i] == n){
		pos = i;
	}
	return pos;
}

//Parâmetro 1º localiza e parâmetro 2º altera.
function alterar(antigo, novo) {
	const indiceAntigo = localizar(antigo);
	if(IsPar(novo)){
		if(indiceAntigo != -1){
			//Remove elemento a partir do índice, e insere um novo valor.
			numeros.splice(indiceAntigo, 1, novo);
			console.log('=> Valor alterado. ')
		} else {
			console.log('=> Valor não existe.');
		}
	}else{
		console.log('O número informado não é par.')
	}
}

/*Opções do sistema:
1 - Inserir um número - ok
2 - Listar os números cadastrados - ok
3 - Excluir um número
4 - Alterar um número do array - ok
5 - Localizar um número no array - ok
0 - Sair
*/

	function ExibeMenu(){
		console.log('\nSistema de controle de números pares. ');
		console.log('\n1 - Inserir um número.  ');
		console.log('2 - Listar os números cadastrados.  ');
		console.log('3 - Excluir um número. ');
		console.log('4 - Alterar um número do array. ');
		console.log('5 - Localizar um número no array. ');
		console.log('0 - Sair. ');
		//Variável local (só vai existe dentro da função).
		let retorno = parseInt(readlineSync.question('\nDigite uma opção: '));
		return retorno;
	}

function jaExiste(lista, n){
	if (lista.includes(n)){
		return true;
	}else{
		return false;
	}
}

while(opmenu != 0){
	opmenu = ExibeMenu();

	if(opmenu == 1){
		Inserir();
	}

	if(opmenu == 2){
		listar();
	}	

	if(opmenu == 4){
		let valorAntigo = parseInt(readlineSync.question('\nDigite um número que deseja substituir: '));
		let valorNovo = parseInt(readlineSync.question('\nDigite o novo valor: '));
		alterar(valorAntigo, valorNovo);
	}

	if(opmenu == 5){
		let n = parseInt(readlineSync.question('\nDigite um número para ser localizado: '));
		// let pos = localizar(n);
		let pos = numeros.indexOf(n);
		if(pos != -1){
			console.log(`\n => Valor ${n} foi encontrado na posição: ${pos}.`);
		}else {
			console.log('\nValor não encontrado.');
		}
	}

}

console.log('\nObrigado por utilizar o nosso sistema.');