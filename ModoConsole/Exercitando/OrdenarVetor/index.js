/*
	ORDENAR VETOR, crie uma opção a mais no menu, na qual o usuário possa 
	ordenar o vetor de números pares.
*/

// Variáveis globais
var readlineSync = require('readline-sync');
var opmenu;
// Vetor
var numeros = new Array();


// Função que devolve um valor booleano true(verdadeiro) ou false(falso)...
function IsPar(n){
	var par = false;
	if(n % 2 == 0){
		par = true;
	}
	return par;
}

// Fazer a leitura do usuário...
function LerNumber(){
	var n = parseInt(readlineSync.question('Digite um número: '));
	return n;
}

// Usando função dentro de outras funções...
function Inserir(){
	// Variável local
	var n = LerNumber();
	// if(IsPar(n) == true)
	if(IsPar(n) == true){
		// Inserindo um número dentro do Array... 
		if((jaExiste(numeros, n))){
			console.log('Erro, esse valor já foi inserido');
		} else {
			numeros.push(n);
		}
	} 
}

function listar(){
	for(var i = 0;i < numeros.length; i++){
		console.log(`[ ${i} ] = ${numeros[i]}`);
	}

}

// function ordenar(numeros){
// 	// A sort ordena os números...
// 	numeros.sort( novaFuncaoOrdenar );
// }

// function novaFuncaoOrdenar(a, b){
// 	// Comparação dos parâmetros...
// 	return a - b;
// }



function ordenar(numeros){
	numeros.sort(function (a, b){
		return a - b;
	});
	console.log('Vetor ordenado!');
}


/*Opções do sistema
1 - Inserir um número ok
2 - Listar os números cadastrados ok

  - Ordenar números (pares).

3 - Excluir um número
4 - Alterar um número do array
5 - Localizar um número no array
0 - Sair
*/

	function ExibeMenu(){
		console.log('Sistema de controle de números pares. ');
		console.log('1 - Inserir um número.  ');
		console.log('2 - Listar os números cadastrados.  ');
		console.log('3 - Excluir um número. ');
		console.log('4 - Alterar um número do array. ');
		console.log('5 - Localizar um número no array. ');
		console.log('6 - Ordenar números (pares). ');
		console.log('0 - Sair. ');
		// Variável local... só vai existir nesta função.
		var retorno = parseInt(readlineSync.question('Digite uma opção: '));
		return retorno;
	}

function jaExiste(lista, n){
	if (lista.includes(n)){
		return true;
	}else{
		return false;
	}
}

while(opmenu != 0){
	opmenu = ExibeMenu();
	if(opmenu == 1){
		Inserir();
	}

	if(opmenu == 2){
		listar();
	}

	if(opmenu == 6){
		ordenar(numeros);
	}

}

console.log('Obrigado por utilizar o nosso sistema.');