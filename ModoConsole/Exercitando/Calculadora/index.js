var readlineSync = require("readline-sync");
let resultados = new Array();

function multiplicar (valor1, valor2) {
  let produto;
  produto = valor1 * valor2;
  return produto;
}
function dividir (dividendo, divisor) {
  let resultado;
  resultado = dividendo / divisor;
  return resultado;
}
function adicao (p1, p2) {
  return p1 + p2;
}
function subtracao (minuendo, subtraendo) {
  return minuendo - subtraendo;
}


function calcular (n1, n2, op) {
  let resultado;
  if (op == "*") {
    resultado = multiplicar(n1, n2);
  } else if (op == "/") {
    resultado = dividir(n1, n2);
  } else if (op == "-") {
    resultado = subtracao(n1, n2);
  } else if (op == "+") {
    resultado = adicao(n1, n2);
  }
  return resultado;
}

/*Primeiro comando solicitado pelo usuario.
O 'do' é um loop que acontece pelo menos uma vez.*/
do {
  var num1 = parseFloat(readlineSync.question('Informe um número 1: '));
  var operacao = readlineSync.question("Informe a operacao[+, -, *, /]: ");
  var num2 = parseFloat(readlineSync.question('Informe um segundo número: '));
  /*num1 = 10;*/
  /*num2 = 2;*/
  let resultado = calcular(num1, num2, operacao);
  console.log(
    `${num1} ${operacao} ${num2} = ${resultado}`
  );
  // typeof: retorna o tipo da variavel. 
  if (typeof resultado === "number") {
    resultados.push(resultado);
  }
  console.log('----------------------------------------------------');
} 
/*Enquanto... execute*/
while (!(num1 == 0 && num2 == 0 && operacao == "0"));

console.log(resultados);

console.log(`Maior: ${Math.max(...resultados)}`);
console.log(`Menor: ${Math.min(...resultados)}`);
console.log('Fim do programa.');