/*
Faça uma função para capturar o APOD e outra para exibir a imagem só que 
agora passando uma data digitada pelo usuário.
*/

import readlineSync from 'readline-sync';
import terminalImage from 'terminal-image';
import got from 'got';

async function showImage(image){
	try {
		const body = await got.get(image).buffer();
		console.log(await terminalImage.buffer(body));
	} catch (error) {
		console.error(error);
	}
}

async function getAPOD(data){
	const dados = await got(
		`https://api.nasa.gov/planetary/apod?api_key=gzQ9oe1KEFYluywSvdYi0Li3jqATMC4vO94vefbf&date=${data}`
		,{}
	);
	let apodImage = dados.body;
	const {url} = JSON.parse(apodImage);
	console.log(url);
	showImage(url);
}

let data;
data = readlineSync.question('Informe a data: ');
getAPOD(data);