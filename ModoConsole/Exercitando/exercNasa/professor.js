const readlineSync = require('readline-sync');
const terminalImage = require('terminal-image');
const got = require('got');

async function showImage(img) {
   const {body} = await got(img, {encoding: null});
   console.log(await terminalImage.buffer(body));
}

async function getApod(date) {
    const data = await got(`https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY&date=${date}`);
    showImage(data.body.url);
}

function read() {
    var date = readlineSync.question('Data da foto: ');
    getApod(date);
}

read();