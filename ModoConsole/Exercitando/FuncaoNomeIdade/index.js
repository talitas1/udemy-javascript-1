/*
Faça uma função que receba por parâmetro seu |nome e sua idade|, pegue o |ano atual| tire a 
diferença e exiba seu nome com seu ano de nascimento concatenados.
*/
function identificar(nome, idade){
	var anoAtual = new Date().getFullYear();
	var anoDeNascimento = anoAtual - idade;
	console.log(`Meu nome é ${nome}, nasci no ano ${anoDeNascimento} e tenho ${idade}.`);
}
var exibir = identificar('Lita', 24)
// identificar(exibir);