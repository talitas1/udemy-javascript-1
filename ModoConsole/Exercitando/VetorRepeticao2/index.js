/*
Faça um programa em que utilizando 2 vetores, no 1º armazene 5 nomes e no 2º 5 notas, ao final exiba o nome do aluno e sua nota desta forma:
Aluno 1: Danilo; nota: 9;
Aluno 2: Vinicius; nota: 10;
*/
var readlineSync = require('readline-sync');
var aluno = [];
var nota = [];

for (var i = 1; i <= 5; i++) {
    aluno[i] = readlineSync.question('Informe o nome do aluno(a): ');    
    nota[i] = parseFloat(readlineSync.question('Informe a nota do aluno(a): '));
    /*variavel i tem o comportamento de contador, ou indice.*/
} 
for (var i = 1; i <= 5; i++) {
    console.log(`Aluno ${i+1}: ${aluno[i]}; Nota: ${nota[i]}`);
    /*console.log sempre é saída.*/
}