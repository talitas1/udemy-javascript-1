/* Faça um programa que leia números inteiros 
e só saia do laço de repetição se o número 0 for digitado.
*/

var readlineSync = require('readline-sync');

console.log('Programa que le numero inteiro, e sai com o comando de decisao while.')

/*Contador*/
var i = 0;
var n = parseInt(readlineSync.question('Digite um numero: '));
/*Enquanto...*/
while (n != 0 ) {
	n = parseInt(readlineSync.question('Digite um numero: '));
	i++;
}