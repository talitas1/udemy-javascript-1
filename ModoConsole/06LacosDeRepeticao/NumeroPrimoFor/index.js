
var readlineSync = require('readline-sync');
var n = 0;
/*número que o usuário irá informar...*/
var count = 0;
/*contador... armazenar quantas vezes conseguir(o resto é igual a 0), dividir o número informado pelo o usuário.*/
var i = 0;
/*utilizar no FOR.*/

console.log('Programa que verifica se um número é primo. ');

/*Leitura dos dados(entrada de dados).*/
n = parseInt(readlineSync.question('Informe um número inteiro: '));

/*Dividir o número N vezes.*/
for (i = 1; i <= n; i++) {
	if (n % i == 0) {
	/*Perguntar, SE n % (por) i for igual a 0... ou melhor se o resto da divisão de n por i for igual a 0.
	Significa que conseguiu dividir o valor armazenado na variável n pelo i. Se isso aconteceu...*/
	count++;
	/*count = count + 1;*/
	}
}

if (count == 2) {
	/*SE count for igual a 2, significa que conseguiu dividir o n apenas 2 vezes, e se isso aconteceu n é um número primo.*/
	console.log(`O número ${n} é um número primo.`);
} else {
	console.log(`O número ${n} NÃO é um número primo.`);
}
