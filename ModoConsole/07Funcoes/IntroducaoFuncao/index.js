/*
1º exemplo: função(function) que não recebe parametro e, não tem retorno(return).*/
function exibirMensagem () {
	console.log("Minha primeira funcao.");
/*console.log é uma função e o que contém dentro dos parenteses () é o parametro da função 'function'.*/
}
/*exibirMensagem();
exibirMensagem();
exibirMensagem();
exibirMensagem();*
/*a function (FUNÇÃO) precisa ser chamada ou invocada para ser executada ou exibida.
*/

	
/*	
2º exemplo: esta por sua vez tem parametro.*/
function exibirMensagemPlus (txt) {
	console.log(txt);
}
/*exibirMensagemPlus('Hello');*/
var valor = 'Primeira aula sobre funções';
/*exibirMensagemPlus(valor);*/


/*3ª exemplo: com parametro e retorno (return).*/
function NumeroPar(num) {
	/*function tem como parametro 'num' que a mesma quer verificar.*/
	var retorno = false;
	/*representa o resultado da verificação. Se 'num' for par o retorno será true, se impar false.*/
	if(num%2 == 0){
		/*indica que o numero é par.*/
		retorno = true;
	}
	return  retorno;
	/*devolver um valor para quem fez a chamada da função (o usuario).*/
}
var par = NumeroPar(4);
console.log(par);