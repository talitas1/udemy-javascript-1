var readlineSync = require("readline-sync");
var n = 0;
/*número que o usuário irá informar...*/
var count = 0;
/*contador... armazenar quantas vezes conseguir(o resto é igual a 0), dividir o número informado pelo o usuário.*/
var i = 0;
/*utilizar no FOR.*/

function numeroPrimo(n) {
  var retorno = false;
  /*Dividir o número N vezes.*/
  for (i = 1; i <= n; i++) {
    if (n % i == 0) {
      /*Perguntar, SE n % (por) i for igual a 0... ou melhor se o resto da divisão de n por i for igual a 0
      // significa que conseguiu dividir o valor armazenado na variável n pelo i. Se isso aconteceu...*/
      count++;
      // count = count + 1;
    }
  }
  if (count == 2) {
    retorno = true;
  }
  return retorno;
}

/*Leitura dos dados(entrada de dados).*/
n = parseInt(readlineSync.question("Informe um número inteiro: "));
var resposta = numeroPrimo(n);

if (resposta == true) {
  /*se resposta for igual a true...*/
  console.log(`O número ${n} é um número primo.`);
} else {
  console.log(`O número ${n} NÃO é um número primo.`);
}